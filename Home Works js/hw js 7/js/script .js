"use strict";


function createNewList(array, parent = document.body){
    let list = array.map((element) => `<li>${element}</li>` );
    parent.insertAdjacentHTML("afterbegin",`<ul>${list.join(" ")}</ul>`);
}
createNewList(["heloo", "world", "kiev", "kharkiv", "odessa", "lviv"]);