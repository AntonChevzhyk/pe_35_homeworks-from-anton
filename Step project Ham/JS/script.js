const listClick = document.querySelectorAll(".services-list-item");
const content = document.querySelectorAll(".services-content");

listClick.forEach(function(item){
     item.addEventListener("click", function(){
		 let activeList = item;
		 let tabId = activeList.getAttribute("data-tab");
		 let currentTab = document.querySelector(tabId);

		 if(!activeList.classList.contains('active')){
			 listClick.forEach(function(item){
			 item.classList.remove('active');
		 });

		 content.forEach(function(item){
			item.classList.remove('active');
		});

		 activeList.classList.add('active');
		 currentTab.classList.add('active');
		 }
		 
	 });
});




let filterBox = document.querySelectorAll(".img");

document.querySelector(".our-amazing-work-list").addEventListener("click", event=>{
	console.log("clicked")
	
	let filterClass = event.target.dataset['f'];
	
	filterBox.forEach(elem =>{
		elem.classList.remove('hide');
	})
	filterBox.forEach( elem => {
		if (!elem.classList.contains(filterClass) && filterClass!== 'all'){
			elem.classList.add('hide');
		}
	})

});




const prev = document.getElementById("btn-prev");
const next = document.getElementById("btn-next");
const slides = document.querySelectorAll(".complite-element");
const dots = document.querySelectorAll(".dots");

let index = 0 ;


const activeSlide = n =>{
	for(slide of slides){
		slide.classList.remove("elem-active")
	}
	slides[n].classList.add("elem-active");
}

const nextSlide = () => {
if(index == slides.length - 1){
	index = 0;
	activeSlide(index);
}else{
	index++;
	activeSlide(index);
}
}

const prevtSlide = () => {
	if(index == 0){
		index = slides.length - 1;
		activeSlide(index);
	}else{
		index--;
		activeSlide(index);
	}
	}

next.addEventListener("click" , nextSlide);
prev.addEventListener("click" , prevtSlide);